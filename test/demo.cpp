#include <gtest/gtest.h>

#include <demolib/demolib.h>

TEST(Demo, FirstDemo)
{
    const auto ans = demolib_add(2, 5);
    EXPECT_EQ(ans, 7);
}

int main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
