# load testing framework
find_package(GTest REQUIRED)

add_executable(demotest
    demo.cpp
)

# link test exe against our library and the test framework
target_link_libraries(demotest PUBLIC GTest::GTest ${PROJECT_NAME})

# teach ctest about our test
add_test(NAME demotest COMMAND demotest --gtest_output=xml:testresults.xml)
