#ifndef DEMOLIB_H
#define DEMOLIB_H

/** \file
 * Interface for demolib.
 */

#include <demolib/demolib_export.h>

#ifdef __cplusplus
extern "C" {
#endif

/** Adds two numbers.
 *
 * Add both numbers without checking for overflow.
 * \return the sum of both parameters.
 */
int DEMOLIB_EXPORT demolib_add(int a, /**< parameter A */ int b /**< Parameter B */);

#ifdef __cplusplus
}
#endif

#endif  // DEMOLIB_H
