# Demo gitlab Repository

Dies ist ein Minimalbeispiel eines git(lab)-Repositories mit einer kleinen shared library (.so/.dll) und einer exe.
Mit einer CI-Pipeline wird in drei Stufen
- die Codeformatierung überprüft (format)
- die Unit-tests ausgeführt sowie die .so an die exe gelinkt (test)
- die Doxygen-Doku gebaut und [hier](https://igh-vh.gitlab.io/gtest-demo/doxygen/) veröffentlicht (docs)

Die config der Pipeline befindet sich in [.gitlab-ci.yml](.gitlab-ci.yml).

Für der Pipeline ihren Status gibt es ein cooles Badge:
[![pipeline status](https://gitlab.com/igh-vh/gtest-demo/badges/master/pipeline.svg)](https://gitlab.com/igh-vh/gtest-demo/-/commits/master)

## Gitlab Quickstart

Auf gitlab.com anmelden (Username `igh-nn`) und Beitritt zur Gruppe [etherlab.org](https://gitlab.com/etherlab.org) beantragen.

Desweiteren sollte ein SSH-Key generiert werden (`ssh-keygen -t rsa`, mit Passphrase versehen) und [hier](https://gitlab.com/-/profile/keys) hochgeladen werden.

## Git und Gitlab

In Gitlab ist die kleinste Einheit ein Projekt, welches genau ein git-Repository besitzt. Jeder Benutzer kann mehrere eigene Projekte haben sowie Mitglied von verschiedenen Gruppen sein. Die Gruppen können ebenfalls Projekte besitzten.


### Workflow für verteilte Versionsverwaltung

An sich ist es angedacht, dass die Weiterentwicklung in getrennten Zweigen geschieht und nach Erreichen der Serienreife in die entsprechenden `master` bzw. `stable`-Zweige gemergt werden.

Damit die Repositories der `etherlab.org`-Gruppe nicht so zugemüllt werden, sollte die Möglichkeit genutzt werden, Projekte zu __forken__. Damit wird eine Kopie des Gruppen-Projekts erstellt und mit diesem verknüpft.

Das Zusammenführen (mergen) funktioniert in Gitlab über __Merge Requests__, auch über geforkte Projekte hinweg. Für den Merge auf `master` bzw. `stable`-Zweige sind Merge requests verpflichtend, für andere Zweige freiwillig.

### Git 101

Arbeitskopie ziehen:
```
# Read-only
git clone https://gitlab.com/igh-vh/gtest-demo.git
# RW (ssh), vorher muss ein SSH-Key hochgeladen werden (s.o.)
git clone git@gitlab.com:igh-vh/gtest-demo.git
```

Neuen Branch lokal erstellen
```
git checkout -b <neuer name>
```

Geänderte Dateien (committen und) hochladen
```
# optional mit flag -p: Änderungen blockweise hinzufügen
git add Datei1 Datei2
git commit -m "my commit message!"
# falls branch noch nie hochgeladen wurde
git push -u origin <branch name>
# falls branch schon existiert
git push
```

Änderungen herunterladen und in Workspace einpflegen
```
git pull
```

Änderungen herunterladen, ohne diese in Workspace einzupflegen
```
git fetch
```

#### Fehler "error: failed to push some refs to ... because the tip of your current branch is behind its remote counterpart.":

Grund: Weitere Commits wurden auf den Branch im entfernten Repository hinzugefügt, während lokal neue Commits erstellt wurden.
Lösung: Die neuen eigenen Commits auf die neuen entfernen commits setzen (rebasen). Dabei wird aber die Geschichte verändert!
```
# Annahme: branch heißt devel
# neue Commits herunterladen
git fetch
# lokale Commits auf die neuen draufsetzen
git rebase origin/devel
# und veröffentlichen
git push
```

### Git Expert area

Branch Wechseln (evtl. mit `git stash` Änderungen wegspeichern, die nicht committet wurden. Kann mit `git stash pop` rückgängig gemacht werden)
```
git checkout <branch name>
```

Zweites Remote-Repository einbinden (z.b. Projekt der Gruppe)
```
git remote add upstream https://gitlab.com/etherlab.org/gtest-demo.git
```

Stable-Branch vom Fork aktualisieren
```
# origin ist der Fork, upstream das Gruppenprojekt
git fetch upstream
# auf eigenen stable-Zweig wechseln
git checkout stable
# eigenen Zweig vorspulen (ohne Merge-Commit)
git merge --ff-only upstream/stable
git push
```
### Issues & Merge Requests

Um Änderungen auf `stable` einzupflegen, muss ein Merge Request erstellt werden. Dazu auf der Gitlab-Projekt-Seite auf `Merge Requests` in der Seitenleiste und dann auf `New`. Dann die Quell- und Zielprojekte sowie -zweige auswählen und anschließend einen Titel sowie eine Beschreibung bzw. Begründung eingeben.

Nachdem ein Review des Merge Requests durch eine andere Person durchgeführt wurde, kann diese dem Merge Request zustimmen (approve). Danach können (sofern die CI durchläuft) die Zweige zusammengeführt werden.

Weiterhin gibt es eine Issue Tracker, in dem Fehlerberichte hinterlegt werden können.

Sowohl Issues als auch Merge Requests können in Commit-Messages oder Kommentaren in Gitlab über deren IDs mit einem `#` bzw. `!` referenziert werden. `#4711` steht also für Issue 4711 und `!57` für Merge request 57.
Weitere Infos zu den Referenzen [hier](https://docs.gitlab.com/ee/user/markdown.html#special-gitlab-references).


## CMake

Leider enthält das CMake-Skript ziemlich viel Boilerplate,
hat aber auch einen sehr großen Funktionsumfang.
Mit den Optionen `BUILD_TESTING` und `BUILD_DOCS` kann gesteuert werden,
ob die Tests bzw. die Doxygen-Dokumentation gebaut werden.
Desweiteren werden für die Beispiel-Bibilothek ein paar Dateien generiert:
- pkg-config Datei
- [.cmake Dateien](https://cmake.org/cmake/help/v3.20/guide/importing-exporting/index.html#creating-packages), um die .so später mit `find_package` in CMake zu importieren
- `version.h`
- Header, welche Makros für zu exportierende Symbole enthält (s.u.)

Desweiteren ist die Beispiel-Bibilothek ein modernes CMake-Target,
kann also über `target_link_library` mit weiteren Targets
verknüpft werden, wobei alle Abhängigkeiten (Include-Pfade und libs)
berücksichtigt werden.

Die Verwendung der Lib ist im Verzeichnis `demoexe` veranschaulicht.

### Exportierte Symbole einer .so

Mit dem Makro `DEMOLIB_EXPORT` werden Funktionen und Klassen markiert,
welche der API der Lib zugehören.
Damit wird erreicht, dass interne Funktionen
nicht von außen benutzt werden können.
Desweiteren wird dadurch unter Windows das Erstellen einer .dll vereinfacht.

Unter Linux expandiert das Makro bei der Kompilierung der .so zu
`__attribute__((visibility("default")))`,
während alle anderen Symbole über ein Compiler-Flag
als `hidden` gesetzt werden. Bei Benutzung der Lib ist das Makro leer,
sodass die Funktionen einfach deklariert werden.

Bei Windows ist das ein wenig komplizierter:
Bei der Kompilierung der .dll werden die API-Symbole mit `__declspec(dllexport)` markiert, sodass diese exportiert werden.
Bei der Benutzung der Lib expandiert es zu `__declspec(dllimport)`,
wodurch die Import-Library der dll überflüssig wird.

## Tipps & Tricks

### CI Benachrichtigungen

Wenn die CI fehlschlägt, gibt's standardmäßig ne E-Mail. Kann man hier ausschalten:

https://gitlab.com/-/profile/notifications

custom -> disable pipeline
