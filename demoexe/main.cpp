#include <demolib/demolib.h>
#include <demolib/config.h>
#include <iostream>

int main()
{
    std::cout << "Demolib major version is " << DEMOLIB_VERSION_MAJOR << "\n";
    const int ans = demolib_add(5, 6);
    std::cout << "5 and 6 is " << ans << "\n";
    return 0;
}
